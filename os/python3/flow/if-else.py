
# 相同縮排則表示了相同區塊範圍的程式碼，縮排必須一致，如果想使用四個空白字元縮排，整個程式都必須是四個空白字元縮排，如果要用 Tab 縮排，整個程式都必須使用 Tab 縮排。

from sys import argv
print( 'Hi, ' + (argv[1] if len(argv) > 1 else 'Guest') ) #   len(argv)>1 ? argv[1] : 'Guest'

# ================================== 
numbers = [11, 2, 45, 1, 6, 3, 7, 8, 9]
odd_numbers = []
even=[]
for num in numbers:
    if num % 2 != 0:
        odd_numbers.append(num)
    else:
        even.append(num)
        
print( odd_numbers , even )

# ---------------------------------------------
numbers = [11, 2, 45, 1, 6, 3, 7, 8, 9]
print( [ number for number in numbers if number % 2 != 0 ])
# ================================== 

>>> mylist = [2,3,4,5,6]
>>> myterm=9
>>> mylist.index(myterm) if myterm in mylist else None 
>>> 9 if myterm > 8 else 0
9
>>> 9 if myterm > 11 else 0
0

>>> a=9
>>> 0 if a is not True else 1
0
>>> a=None
>>> 0 if a is not True else 1
0
>>> a='True'
>>> 0 if a is not True else 1
0
>>> a=True
>>> 0 if a is not True else 1
1


>>> 'T' if (3>4) else 'F'
'F'
>>> 'T' if (5>4) else 'F'
'T'






