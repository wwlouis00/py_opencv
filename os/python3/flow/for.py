
# 相同縮排則表示了相同區塊範圍的程式碼，縮排必須一致，如果想使用四個空白字元縮排，整個程式都必須是四個空白字元縮排，如果要用 Tab 縮排，整個程式都必須使用 Tab 縮排。

# ================================== 
numbers = [10, 20, 30]
squares = []
for number in numbers:
    squares.append(number ** 2)
print( squares)
# ---------------------------------------------
numbers = [10, 20, 30]
print( [ number ** 2 for number in numbers ])
# ================================== 
numbers = [11, 2, 45, 1, 6, 3, 7, 8, 9]
odd_numbers = []
even=[]
for num in numbers:
    if num % 2 != 0:
        odd_numbers.append(num)
    else:
        even.append(num)
        
print( odd_numbers , even )

# ---------------------------------------------
numbers = [11, 2, 45, 1, 6, 3, 7, 8, 9]
print( [ number for number in numbers if number % 2 != 0 ])
# ================================== 
numbers = []
for number in range(20):
    numbers.append(str(number))
print( ", ".join(numbers))
# ---------------------------------------------
print(  ", ".join([ str(number) for number in range(20) ]))
# ================================== 


lts = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print( [ele for lt in lts for ele in lt])

print( {name for name in ['caterpillar', 'Justin', 'caterpillar', 'openhome']})
print( [name for name in ['caterpillar', 'Justin', 'caterpillar', 'openhome']])

names = ['caterpillar', 'justin', 'openhome']
passwds = [123456, 654321, 13579]
print(  {name : passwd for name, passwd in zip(names, passwds)})
# zip 函式，會將兩個 list 的元素兩兩相扣在一起為 tuple，這些 tuple 元素組成一個新的 list，每個 tuple 中的一對元素再指定給 name 與 passwd，最後組成 dict 的一對鍵值。 
print(  [(name , passwd) for name, passwd in zip(names, passwds)])
print(  zip(names, passwds))










